<?php ?>
<!DOCTYPE html>  
<html>  
<head>  
<style>  
.error {color: #FF0001;}  


</style>
<link rel = "stylesheet" type = "text/css" href = "style.css">  
</head>  
<body>    
  
<?php 

include('connection.php');

// define variables to empty values  
$nameErr = $passwordErr= $emailErr= $mobilenoErr = "";  
$name =$password =  $email = $mobileno  = "";  
  
//Input fields validation  
if ($_SERVER["REQUEST_METHOD"] == "POST") {  
      
//String Validation  
    if (empty($_POST["name"])) {  
         $nameErr = "Name is required";  
    } else {  
        $name = input_data($_POST["name"]);  
            // check if name only contains letters and whitespace  
            if (!preg_match("/^[a-zA-Z ]*$/",$name)) {  
                $nameErr = "Only alphabets and white space are allowed";  
            }  
    }
    
    if (empty($_POST["password"])) {
        $passwordErr = "password is required";
    } else {
        $password = input_data($_POST["password"]);
        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z ]*$/",$password)) {
            $passwordErr = "Only alphabets and white space are allowed";
        }
    }
    
      
    //Email Validation   
    if (empty($_POST["email"])) {  
            $emailErr = "Email is required";  
    } else {  
            $email = input_data($_POST["email"]);  
            // check that the e-mail address is well-formed  
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {  
                $emailErr = "Invalid email format";  
            }  
     }  
     
     
    
    //Number Validation  
     if (empty($_POST["mobileno"])) {  
            $mobilenoErr = "Mobile no is required";  
    } else {  
            $mobileno = input_data($_POST["mobileno"]);  
            // check if mobile no is well-formed  
            if (!preg_match ("/^[0-9]*$/", $mobileno) ) {  
            $mobilenoErr = "Only numeric value is allowed.";  
            }  
        //check mobile no length should not be less and greator than 10  
        if (strlen ($mobileno) != 10) {  
            $mobilenoErr = "Mobile no must contain 10 digits.";  
            }  
    }  
      
      
      
}  
function input_data($data) {  
  $data = trim($data);  
  $data = stripslashes($data);  
  $data = htmlspecialchars($data);  
  return $data;  
}  
?>  
  
<div id = "frm">
<h2>Registration Form</h2>  
<span class = "error">* required field </span>  
<br><br>
  
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" >    
    Name:   
    <input type="text" name="name">  
    <span class="error">* <?php echo $nameErr; ?> </span>  
    <br><br>
    Password:   
    <input type="text" name="password">  
    <span class="error">* <?php echo $passwordErr; ?> </span>  
    <br><br>
      
    E-mail:   
    <input type="text" name="email">  
    <span class="error">* <?php echo $emailErr; ?> </span>  
    <br><br>  
	 Mobile No:   
    <input type="text" name="mobileno">  
    <span class="error">* <?php echo $mobilenoErr; ?> </span>  
    <br><br>  
     
    <br><br>                            
    <input type="submit" name="submit" id = "btn" value="Submit">   
    <br><br>                             
</form>  
  </div>
<?php  
    if(isset($_POST['submit'])) {  
        
      /*  echo "Name: " .$nameErr;
        echo "Password: " .$passwordErr;
        
        echo "Email: " .$emailErr;
        echo "Mobile No: " .$mobilenoErr;*/
        
        if($name != "" && $password != "" && $email != "" && $mobileno != "") { 
        
        $sql="insert into users(username,email,password, mobileno) values('".$_REQUEST['name']."','".$_REQUEST['email']."', '".$_REQUEST['password']."', '".$_REQUEST['mobileno']."')";
        $res=mysqli_query($con,$sql);
        
        If($res)
        {
            Echo "Record successfully inserted";
        }
        Else
        {
            Echo "There is some problem in inserting record";
        }
             
    } else {  
        echo "<h3> <b>You didn't filled up the form correctly.</b> </h3>";  
    }  
    }  
?>  
  
</body>  
</html> 